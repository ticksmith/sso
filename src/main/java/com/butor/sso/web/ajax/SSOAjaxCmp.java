/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.sso.web.ajax;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.protocol.HTTP;
import org.butor.auth.common.AuthMessageID;
import org.butor.auth.common.firm.Firm;
import org.butor.auth.common.user.User;
import org.butor.auth.common.user.UserKey;
import org.butor.auth.common.user.UserQuestions;
import org.butor.checksum.CommonChecksumFunction;
import org.butor.json.CommonRequestArgs;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandler;
import org.butor.mail.IMailer;
import org.butor.sso.AuthInfoProvider;
import org.butor.sso.SSOConstants;
import org.butor.sso.SSOHelper;
import org.butor.sso.SSOInfo;
import org.butor.sso.SSOManager;
import org.butor.sso.UserInfoProvider;
import org.butor.utils.AccessMode;
import org.butor.utils.ApplicationException;
import org.butor.utils.CommonMessageID;
import org.butor.utils.StringUtil;
import org.butor.web.servlet.AjaxContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import com.butor.portal.common.PortalMessageID;
import com.butor.portal.common.login.ChangePwdCredential;
import com.butor.portal.common.login.Credential;
import com.butor.portal.common.login.QR;
import com.butor.portal.common.login.UserRegistration;
import com.google.common.base.Strings;

public class SSOAjaxCmp {
	public static final String DEFAULT_EMAIL_REGEX = "^[\\w\\.-]+@[\\w\\.-]+\\.[\\w\\.-]+$";

	private Logger logger = LoggerFactory.getLogger(getClass());
	protected SSOManager ssoManager = null;
	protected UserInfoProvider userInfoProvider = null;
	protected AuthInfoProvider authInfoProvider = null;
	private String emailRegEx = DEFAULT_EMAIL_REGEX;
	private IMailer mailer;
	private String fromRecipient;
	private String adminNotifEmails;
	private final String SEC_SYSTEM = "sec";
	private String registrationDisabledDomains;
	private Pattern registrationDisabledDomainsPattern = null;

	public SSOAjaxCmp() {
		super();
	}

	protected boolean validateEmail(String email) {
		if (StringUtil.isEmpty(email)) {
			return false;
		}
		return email.matches(emailRegEx);
	}

	private boolean isRegistrationAllowed(String domain) {
		if (Strings.isNullOrEmpty(domain)) {
			return false;
		}
		if (Strings.isNullOrEmpty(registrationDisabledDomains)) {
			return false;
		}

		if (registrationDisabledDomainsPattern == null) {
			registrationDisabledDomainsPattern = Pattern.compile(registrationDisabledDomains);
		}

		Matcher matcher = registrationDisabledDomainsPattern.matcher(domain);
		return !matcher.find();
	}

	public void getUserInfo(final Context<Map<String, Object>> ctx) {
		final ResponseHandler<Map<String, Object>> resp = ctx.getResponseHandler();

		Map<String, Object> info = new HashMap<String, Object>();
		info.put("env", System.getProperty("env"));

		AjaxContext ac = (AjaxContext) ctx;
		String ssoId = SSOHelper.getCookie(ac.getHttpServletRequest(), SSOConstants.SSO_SSO_ID);
		HttpSession hts = ac.getHttpServletRequest().getSession(true);
		SSOInfo sos = ssoManager.getSSOSession(ssoId);

		if (sos != null) {
			// if (hts == null || hts.getAttribute(SSOConstants.SSO_SSO_ID) ==
			// null) {
			// recreate session if sso still valid
			hts = ac.getHttpServletRequest().getSession(true);
			hts.setAttribute(SSOConstants.SSO_ID, sos.getId());
			hts.setAttribute(SSOConstants.SSO_SSO_ID, ssoId);
			// }
			User u = userInfoProvider.readUser(sos.getId(), ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
					ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
			if (u != null && checkDomainAccess(u.getFirmId(), ac)) {
				boolean twoStepsSignin = isTwoStepsSignin(u.getFirmId(), ac);
				info.put("firmId", u.getFirmId());
				info.put("theme", u.getTheme());
				info.put("firmName", u.getFirmName());
				info.put("email", u.getEmail());
				info.put("user", sos);
				info.put("twoStepsSignin", twoStepsSignin);
			}
		}

		resp.addRow(info);
	}

	public void generateApi(final Context<String> ctx) {
		// session
		AjaxContext ac = (AjaxContext) ctx;
		String ssoId = SSOHelper.getCookie(ac.getHttpServletRequest(), SSOConstants.SSO_SSO_ID);
		HttpSession hts = ac.getHttpServletRequest().getSession(true);
		SSOInfo sos = ssoManager.getSSOSession(ssoId);
		if (sos == null) {
			ApplicationException.exception(PortalMessageID.LOGIN_TIMEOUT.getMessage());
		}
		hts = ac.getHttpServletRequest().getSession(true);
		hts.setAttribute(SSOConstants.SSO_ID, sos.getId());
		hts.setAttribute(SSOConstants.SSO_SSO_ID, ssoId);
		// check user security
		boolean hasAccess = authInfoProvider.hasAccess(SEC_SYSTEM, "apiKey", AccessMode.WRITE, sos.getId(),
				ac.getRequest().getSessionId(), ac.getRequest().getReqId(), ac.getRequest().getLang(),
				getDomainName(ac.getHttpServletRequest()));
		if (!hasAccess) {
			ApplicationException.exception(PortalMessageID.GENERATE_APIKEY_FAILED.getMessage("UNAUTHORIZED"));
		}
		// retrieve user
		User user = userInfoProvider.readUser(sos.getId(), ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (user == null) {
			logger.warn("Unknown user {}", sos.getId());
			ApplicationException.exception(PortalMessageID.GENERATE_APIKEY_FAILED.getMessage());
		}
		// save new key to user
		String newKey = CommonChecksumFunction.SHA256.generateChecksum(sos.getId());
		user.setAttribute("apiKey", newKey);
		// save user
		UserKey uk = userInfoProvider.updateUser(user, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (uk.getId() == null) {
			logger.warn("Failed to save new Api key for user={}", sos.getId());
			throw ApplicationException.exception(PortalMessageID.GENERATE_APIKEY_FAILED.getMessage());
		}
		// return
		final ResponseHandler<String> resp = ctx.getResponseHandler();
		resp.addRow(newKey);
	}

	public void getApi(final Context<String> ctx) {
		// session
		AjaxContext ac = (AjaxContext) ctx;
		String ssoId = SSOHelper.getCookie(ac.getHttpServletRequest(), SSOConstants.SSO_SSO_ID);
		HttpSession hts = ac.getHttpServletRequest().getSession(true);
		SSOInfo sos = ssoManager.getSSOSession(ssoId);
		if (sos == null) {
			ApplicationException.exception(PortalMessageID.LOGIN_TIMEOUT.getMessage());
		}
		hts = ac.getHttpServletRequest().getSession(true);
		hts.setAttribute(SSOConstants.SSO_ID, sos.getId());
		hts.setAttribute(SSOConstants.SSO_SSO_ID, ssoId);

		// check user security
		boolean hasAccess = authInfoProvider.hasAccess(SEC_SYSTEM, "apiKey", AccessMode.WRITE, sos.getId(),
				ac.getRequest().getSessionId(), ac.getRequest().getReqId(), ac.getRequest().getLang(),
				getDomainName(ac.getHttpServletRequest()));
		if (!hasAccess) {
			ApplicationException.exception(PortalMessageID.GET_APIKEY_FAILED.getMessage("UNAUTHORIZED"));
		}
		// retrieve user
		User user = userInfoProvider.readUser(sos.getId(), ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (user == null) {
			logger.warn("Unknown user with id={}", sos.getId());
			ApplicationException.exception(PortalMessageID.GET_APIKEY_FAILED.getMessage());
		}
		// retrieve key from user
		String key = (String) user.getAttribute("apiKey");
		final ResponseHandler<String> resp = ctx.getResponseHandler();
		resp.addRow(key);
	}

	public void removeApi(final Context<Boolean> ctx) {
		// session
		AjaxContext ac = (AjaxContext) ctx;
		String ssoId = SSOHelper.getCookie(ac.getHttpServletRequest(), SSOConstants.SSO_SSO_ID);
		HttpSession hts = ac.getHttpServletRequest().getSession(true);
		SSOInfo sos = ssoManager.getSSOSession(ssoId);
		if (sos == null) {
			ApplicationException.exception(PortalMessageID.LOGIN_TIMEOUT.getMessage());
		}
		hts = ac.getHttpServletRequest().getSession(true);
		hts.setAttribute(SSOConstants.SSO_ID, sos.getId());
		hts.setAttribute(SSOConstants.SSO_SSO_ID, ssoId);
		// check user security
		boolean hasAccess = authInfoProvider.hasAccess(SEC_SYSTEM, "apiKey", AccessMode.WRITE, sos.getId(),
				ac.getRequest().getSessionId(), ac.getRequest().getReqId(), ac.getRequest().getLang(),
				getDomainName(ac.getHttpServletRequest()));
		if (!hasAccess) {
			ApplicationException.exception(PortalMessageID.REMOVE_APIKEY_FAILED.getMessage("UNAUTHORIZED"));
		}
		// retrieve user
		User user = userInfoProvider.readUser(sos.getId(), ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (user == null) {
			logger.warn("Unknown user with id={}", sos.getId());
			ApplicationException.exception(PortalMessageID.REMOVE_APIKEY_FAILED.getMessage());
		}
		user.setAttribute("apiKey", null);

		// update user
		UserKey uk = userInfoProvider.updateUser(user, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (uk.getId() == null) {
			logger.warn("Failed to remove Api key for user={}", sos.getId());
			throw ApplicationException.exception(PortalMessageID.REMOVE_APIKEY_FAILED.getMessage());
		}
		final ResponseHandler<Boolean> resp = ctx.getResponseHandler();
		resp.addRow(true);
	}

	public void changePwd(final Context<?> ctx, final ChangePwdCredential cred) {
		final ResponseHandler<?> resp = ctx.getResponseHandler();

		AjaxContext ac = (AjaxContext) ctx;

		if (cred == null) {
			logger.warn("Missing credential arg");
			throw ApplicationException.exception(PortalMessageID.CHANGE_PWD_FAILED.getMessage());
		}
		if (StringUtil.isEmpty(cred.getId())) {
			logger.warn("Missing credential id arg");
			throw ApplicationException.exception(PortalMessageID.CHANGE_PWD_FAILED.getMessage());
		}

		if (StringUtil.isEmpty(cred.getPwd()) && StringUtil.isEmpty(cred.getToken())) {
			logger.warn("Missing credential token arg for user={}", cred.getId());
			throw ApplicationException.exception(PortalMessageID.CHANGE_PWD_FAILED.getMessage());
		}
		if (StringUtil.isEmpty(cred.getNewPwd())) {
			logger.warn("Missing credential new pwd arg for user={}", cred.getId());
			throw ApplicationException.exception(PortalMessageID.CHANGE_PWD_FAILED.getMessage());
		}
		if (StringUtil.isEmpty(cred.getNewPwdConf())) {
			logger.warn("Missing credential new pwd confirmation arg for user={}", cred.getId());
			throw ApplicationException.exception(PortalMessageID.CHANGE_PWD_FAILED.getMessage());
		}
		if (StringUtil.isEmpty(cred.getKaptcha())) {
			logger.warn("Missing credential kaptcha for user={}", cred.getId());
			throw ApplicationException.exception(PortalMessageID.CHANGE_PWD_FAILED.getMessage());
		}
		if (!cred.getNewPwd().equals(cred.getNewPwdConf())) {
			logger.warn("Password confirmation mismatch for user={}", cred.getId());
			throw ApplicationException.exception(PortalMessageID.CHANGE_PWD_FAILED.getMessage());
		}

		// kaptcha first
		String kaptcha = getExpectedKaptcha(ac.getHttpServletRequest());
		if (StringUtil.isEmpty(kaptcha)) {
			logger.warn("Missing generated kaptcha in session for user={}", cred.getId());
			throw ApplicationException.exception(PortalMessageID.CHANGE_PWD_FAILED.getMessage());
		}
		if (!kaptcha.equals(cred.getKaptcha())) {
			logger.warn(String.format("Kaptchas mismatched for user=%s. Expected \"%s\" but got \"%s\"", cred.getId(),
					kaptcha, cred.getKaptcha()));
			throw ApplicationException.exception(PortalMessageID.CHANGE_PWD_FAILED.getMessage());
		}

		if (logger.isDebugEnabled())
			logger.debug(cred.toString());

		User user = userInfoProvider.readUser(cred.getId(), ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (user == null) {
			logger.warn("Unknown user with id={}", cred.getId());
			ApplicationException.exception(PortalMessageID.CHANGE_PWD_FAILED.getMessage());
		}
		if (StringUtil.isEmpty(user.getEmail())) {
			logger.warn("No email defined for user={}", cred.getId());
			resp.addMessage(PortalMessageID.CHANGE_PWD_FAILED.getMessage());
			return;
		}

		// check DB hashed password
		if (!StringUtil.isEmpty(cred.getPwd())) {
			if (!CommonChecksumFunction.SHA512.validateChecksum(cred.getPwd(), user.getPwd())) {
				logger.warn("Password do not match for user={}", cred.getId());
				throw ApplicationException.exception(PortalMessageID.CHANGE_PWD_FAILED.getMessage());
			}
		} else {
			if (!cred.getToken().equals(user.getPwd())) {
				logger.warn("Bad change pwd token for user={}", cred.getId());
				throw ApplicationException.exception(PortalMessageID.CHANGE_PWD_FAILED.getMessage());
			}
		}
		user.setPwd(CommonChecksumFunction.SHA512.generateChecksum(cred.getNewPwd()));
		user.setActive(true);
		user.setMissedLogin(0);

		UserKey uk = userInfoProvider.updateUser(user, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (uk.getId() == null) {
			logger.warn("Failed to save new password for user={}", cred.getId());
			throw ApplicationException.exception(PortalMessageID.CHANGE_PWD_FAILED.getMessage());
		}

		HttpSession hts = ac.getHttpServletRequest().getSession(true);
		boolean forceSignin = (hts.getAttribute("forceSignin") != null);
		
		String ssoId = SSOHelper.getCookie(ac.getHttpServletRequest(), SSOConstants.SSO_SSO_ID);
		if (!forceSignin && ssoId == null || ssoManager.getSSOSession(ssoId) == null) {
			// create session
			if (isTwoStepsSignin(user.getFirmId(), ac)) {
				UserQuestions uq = userInfoProvider.readUserQuestions(user.getId(), ac.getRequest().getSessionId(),
						ac.getRequest().getReqId(), ac.getRequest().getLang(),
						getDomainName(ac.getHttpServletRequest()));
				if (user.isResetInProgress() || Strings.isNullOrEmpty(uq.getQ1()) || Strings.isNullOrEmpty(uq.getR1())
						|| Strings.isNullOrEmpty(uq.getQ2()) || Strings.isNullOrEmpty(uq.getR2())
						|| Strings.isNullOrEmpty(uq.getQ3()) || Strings.isNullOrEmpty(uq.getR3())) {

					hts.setAttribute("id", user.getId());
					resp.addMessage(PortalMessageID.QUESTIONS_SETUP_REQUIRED.getMessage());
					return;
				}
			}

			ssoId = createSSOSession(hts, user, ac);
		}
		
		if (forceSignin) {
			if (ssoId != null) {
				this.ssoManager.destroySSOSession(ssoId);
			}
		} else {
			resp.addMessage(PortalMessageID.CHANGE_PWD_SUCCESS.getMessage());
		}
	}

	public void resetPwd(final Context<?> ctx, final ChangePwdCredential cred) {
		final ResponseHandler<?> resp = ctx.getResponseHandler();

		AjaxContext ac = (AjaxContext) ctx;

		if (cred == null) {
			logger.warn("Missing credential arg");
			throw ApplicationException.exception(PortalMessageID.RESET_PWD_FAILED.getMessage());
		}
		if (StringUtil.isEmpty(cred.getId())) {
			logger.warn("Missing credential id arg");
			throw ApplicationException.exception(PortalMessageID.RESET_PWD_FAILED.getMessage());
		}
		if (StringUtil.isEmpty(cred.getKaptcha())) {
			logger.warn("Missing credential kaptcha for user={}", cred.getId());
			throw ApplicationException.exception(PortalMessageID.RESET_PWD_FAILED.getMessage());
		}

		// kaptcha first
		String kaptcha = getExpectedKaptcha(ac.getHttpServletRequest());
		if (StringUtil.isEmpty(kaptcha)) {
			logger.warn("Missing generated kaptcha in session for user={}", cred.getId());
			throw ApplicationException.exception(PortalMessageID.RESET_PWD_FAILED.getMessage());
		}
		if (!kaptcha.equals(cred.getKaptcha())) {
			logger.warn(String.format("Kaptchas mismatched for user=%s. Expected \"%s\" but got \"%s\"", cred.getId(),
					kaptcha, cred.getKaptcha()));
			throw ApplicationException
					.exception(PortalMessageID.RESET_PWD_FAILED.getMessage());
		}

		userInfoProvider.resetLogin(cred.getId(), getSSOUrl(ac.getHttpServletRequest()), true,
				ac.getRequest().getSessionId(), ac.getRequest().getReqId(), ac.getRequest().getLang(),
				getDomainName(ac.getHttpServletRequest()));

		String ssoId = SSOHelper.getCookie(ac.getHttpServletRequest(), SSOConstants.SSO_SSO_ID);
		if (ssoId != null) {
			this.ssoManager.destroySSOSession(ssoId);
		}
		resp.addMessage(PortalMessageID.RESET_PWD_SUCCESS.getMessage());
	}

	protected String getExpectedKaptcha(HttpServletRequest req) {
		HttpSession hts = req.getSession(false);
		if (hts == null) {
			return null;
		}
		// single use of kaptcha
		// retrieve and delete from session
		String kaptcha = (String) hts.getAttribute(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);
		hts.removeAttribute(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);
		return kaptcha;
	}
	protected String getSSOUrl(HttpServletRequest req) {
		String url = req.getRequestURL().toString();
		String cp = req.getContextPath();
		int pos = url.indexOf(cp);
		return url.substring(0, pos + cp.length());
	}

	protected String getSSODomain(HttpServletRequest req) {
		return getDomainName(req);
	}

	public void activate(final Context<?> ctx, final ChangePwdCredential cred) {
		final ResponseHandler<?> resp = ctx.getResponseHandler();

		AjaxContext ac = (AjaxContext) ctx;

		if (cred == null) {
			logger.warn("Missing credential arg");
			throw ApplicationException.exception(PortalMessageID.ACTIVATE_ACCOUNT_FAILED.getMessage());
		}
		// cred.setId(ac.getHttpServletRequest().getSession().getId());
		if (StringUtil.isEmpty(cred.getId())) {
			logger.warn("Missing credential id arg");
			throw ApplicationException.exception(PortalMessageID.ACTIVATE_ACCOUNT_FAILED.getMessage());
		}
		if (StringUtil.isEmpty(cred.getToken())) {
			logger.warn("Missing credential token for user={}", cred.getId());
			throw ApplicationException.exception(PortalMessageID.ACTIVATE_ACCOUNT_FAILED.getMessage());
		}
		if (StringUtil.isEmpty(cred.getKaptcha())) {
			logger.warn("Missing credential kaptcha for user={}", cred.getId());
			throw ApplicationException.exception(PortalMessageID.ACTIVATE_ACCOUNT_FAILED.getMessage());
		}

		// kaptcha first
		String kaptcha = getExpectedKaptcha(ac.getHttpServletRequest());
		if (StringUtil.isEmpty(kaptcha)) {
			logger.warn("Missing generated kaptcha in session for user={}", cred.getId());
			throw ApplicationException.exception(PortalMessageID.ACTIVATE_ACCOUNT_FAILED.getMessage());
		}
		if (!kaptcha.equals(cred.getKaptcha())) {
			logger.warn(String.format("Kaptchas mismatched for user=%s. Expected \"%s\" but got \"%s\"", cred.getId(),
					kaptcha, cred.getKaptcha()));
			throw ApplicationException
					.exception(PortalMessageID.ACTIVATE_ACCOUNT_FAILED.getMessage());
		}

		if (logger.isDebugEnabled())
			logger.debug(cred.toString());

		User user = userInfoProvider.readUser(cred.getId(), ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (user == null) {
			logger.warn("Unknown user {}", cred.getId());
			ApplicationException.exception(PortalMessageID.ACTIVATE_ACCOUNT_FAILED.getMessage());
		}
		if (StringUtil.isEmpty(user.getEmail())) {
			logger.warn("No user defined for user={}", cred.getId());
			resp.addMessage(PortalMessageID.ACTIVATE_ACCOUNT_FAILED.getMessage());
			return;
		}

		// the reset token was stored in the pwd field
		if (!cred.getToken().equals(user.getPwd())) {
			logger.warn("Bad token provided for user={}", cred.getId());
			resp.addMessage(PortalMessageID.ACTIVATE_ACCOUNT_FAILED.getMessage());
			return;
		}

		user.setActive(true);
		user.setResetInProgress(true);
		user.setMissedLogin(0);

		UserKey uk = userInfoProvider.updateUser(user, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (uk.getId() == null) {
			logger.warn("Failed to update user on activation for user={}", cred.getId());
			throw ApplicationException.exception(PortalMessageID.ACTIVATE_ACCOUNT_FAILED.getMessage());
		}
	}

	public void register(final Context<?> ctx, final UserRegistration ur) {
		final CommonRequestArgs cra = ctx.getRequest();
		String domain = cra.getDomain();
		boolean isRegistrationAllowed = isRegistrationAllowed(domain);
		if (!isRegistrationAllowed) {
			logger.warn("Registration not allowed on domain={}", domain);
			throw ApplicationException.exception(PortalMessageID.REGISTRATION_FAILED.getMessage());
		}

		final ResponseHandler<?> resp = ctx.getResponseHandler();
		AjaxContext ac = (AjaxContext) ctx;

		if (ur == null) {
			logger.warn("Missing user arg");
			throw ApplicationException.exception(PortalMessageID.REGISTRATION_FAILED.getMessage());
		}
		if (StringUtil.isEmpty(ur.getEmail())) {
			logger.warn("Missing user email arg");
			throw ApplicationException.exception(PortalMessageID.REGISTRATION_FAILED.getMessage());
		}
		if (!validateEmail(ur.getEmail())) {
			logger.warn("Invalid user email={}", ur.getEmail());
			throw ApplicationException.exception(CommonMessageID.INVALID_ARG.getMessage("email"));
		}
		if (StringUtil.isEmpty(ur.getKaptcha())) {
			logger.warn("Missing kaptcha for user={}", ur.getEmail());
			throw ApplicationException.exception(PortalMessageID.REGISTRATION_FAILED.getMessage());
		}

		// kaptcha first 
		String kaptcha = getExpectedKaptcha(ac.getHttpServletRequest());
		if (StringUtil.isEmpty(kaptcha)) {
			logger.warn("Missing generated kaptcha in session for user={}", ur.getEmail());
			throw ApplicationException.exception(PortalMessageID.REGISTRATION_FAILED.getMessage());
		}
		if (!kaptcha.equals(ur.getKaptcha())) {
			logger.warn(String.format("Kaptchas mismatched for user=%s. Expected \"%s\" but got \"%s\"", ur.getEmail(),
					kaptcha, ur.getKaptcha()));
			throw ApplicationException
					.exception(PortalMessageID.REGISTRATION_FAILED.getMessage());
		}

		if (logger.isDebugEnabled())
			logger.debug(ur.toString());

		User user = userInfoProvider.readUser(ur.getEmail(), ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (user != null) {
			logger.warn("User with email={} already registered!", ur.getEmail());
			resp.addMessage(PortalMessageID.REGISTRATION_FAILED.getMessage());
			return;
		}

		user = new User();
		BeanUtils.copyProperties(ur, user);
		user.setId(user.getEmail());
		user.setDisplayName(user.getFirstName() + " " + user.getLastName());
		user.setFirmId(2); // 2 reserved for new unassigned users
		user.setLanguage("en");
		String tokSeed = System.currentTimeMillis() + "";

		// gen token for reset
		String resetToken = CommonChecksumFunction.SHA256.generateChecksumWithTTL(tokSeed, 2, TimeUnit.HOURS);
		user.setPwd(resetToken);
		user.setActive(false);
		user.setResetInProgress(true);
		user.setMissedLogin(0);
		
		user.setAttribute("ip", ac.getHttpServletRequest().getRemoteAddr());
		user.setAttribute("domain", getDomainName(ac.getHttpServletRequest()));
		String userAgent = ac.getHttpServletRequest().getHeader(HTTP.USER_AGENT);
		if (userAgent == null) {
			userAgent = "";
		}
		user.setAttribute("userAgent", userAgent);
		try {
			user.setAttribute("reverseLookup",
					InetAddress.getByName(ac.getHttpServletRequest().getRemoteAddr()).getCanonicalHostName());
		} catch (UnknownHostException e) {
			// Nothing to do
		}

		UserKey uk = userInfoProvider.insertUser(user, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (uk.getId() == null) {
			logger.warn("Failed to register (create) user={}", ur.getEmail());
			throw ApplicationException.exception(PortalMessageID.REGISTRATION_FAILED.getMessage());
		}

		String url = getSSOUrl(ac.getHttpServletRequest());

		String msg = "Your login to the portal has been created as you requested.\n Please click on the link bellow and follow the instructions.\n\n";
		msg += "Votre login au portail a été créé à votre demande.\n SVP cliquer sur le lien plus bas et suivre les instructions.\n\n";
		msg += url + "/reset?t=" + resetToken + "&id=" + user.getId();

		logger.info("Sending activation link via email for user={}\n{}", ur.getEmail(), msg);

		String rcpt = user.getEmail();
		mailer.sendMail(rcpt, "New user registered on " + url, msg, fromRecipient);

		msg = "User " + user.getEmail() + " has registered on " + url + ". Please handle him with care.";
		mailer.sendMail(adminNotifEmails, "User registered", msg, fromRecipient);
		String ssoId = SSOHelper.getCookie(ac.getHttpServletRequest(), SSOConstants.SSO_SSO_ID);
		if (ssoId != null) {
			this.ssoManager.destroySSOSession(ssoId);
		}
		resp.addMessage(PortalMessageID.REGISTRATION_SUCCESS.getMessage());
	}

	public void setSsoManager(SSOManager ssoManager) {
		this.ssoManager = ssoManager;
	}

	public void setEmailRegEx(String emailRegEx) {
		this.emailRegEx = emailRegEx;
	}

	public void setMailer(IMailer mailer) {
		this.mailer = mailer;
	}

	public void signInS1(final Context<Map<String, Object>> ctx, final Credential cred) {
		if (cred == null) {
			logger.warn("Missing credential arg");
			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}
		if (StringUtil.isEmpty(cred.getId())) {
			logger.warn("Missing credential id arg");
			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}
		if (StringUtil.isEmpty(cred.getPwd())) {
			logger.warn("Missing credential pwd arg for user={}", cred.getId());
			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}

		if (logger.isDebugEnabled()) {
			logger.debug(cred.toString());
		}

		String id = cred.getId();

		AjaxContext ac = (AjaxContext) ctx;

		final ResponseHandler<Map<String, Object>> resp = ctx.getResponseHandler();

		User user = userInfoProvider.readUser(id, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (user == null) {
			logger.warn("Unknown user {}", id);
			resp.addMessage(AuthMessageID.LOGIN_FAILED.getMessage());
			return;
		} else {
			if (user.getMissedLogin() >= 3) {
				logger.warn("Missed logins >= 3 for user={}", id);
				throw ApplicationException.exception(PortalMessageID.LOGIN_FAILED.getMessage());
			}
			if (!user.isActive()) {
				logger.warn("Login not active for user={}", id);
				resp.addMessage(PortalMessageID.LOGIN_FAILED.getMessage());
				return;
			}
		}
		// check hashed password
		if (!CommonChecksumFunction.SHA512.validateChecksum(cred.getPwd(), user.getPwd())) {
			logger.warn("Password do not match for user={}", id);

			user.setMissedLogin(user.getMissedLogin() + 1);
			UserKey uk = userInfoProvider.updateState(user, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
					ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
			if (uk.getId() == null) {
				logger.warn("Failed to lock login account after 3 unsuccessful login attempts for user={}", id);
				throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
			}

			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}

		if (!checkDomainAccess(user.getFirmId(), ac)) {
			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}

		HttpSession hts = ac.getHttpServletRequest().getSession(true);
		hts.setAttribute("id", cred.getId());
		hts.setAttribute("signInStep", "1");

		boolean twoStepsSignin = isTwoStepsSignin(user.getFirmId(), ac);
		Map<String, Object> result = new HashMap<String, Object>();
		if (!twoStepsSignin) {
			// ignore challenge questions
			logger.info("User's firm has disabled Challenge questions signin!");

			result.put("twoStepsSignin", Boolean.FALSE);

			user.setLastLoginDate(new Date());
			user.setMissedLogin(0);
			user.setAttribute("ip", ac.getHttpServletRequest().getRemoteAddr());
			user.setAttribute("domain", getDomainName(ac.getHttpServletRequest()));
			String userAgent = ac.getHttpServletRequest().getHeader(HTTP.USER_AGENT);

			if (userAgent == null) {
				userAgent = "";
			}
			user.setAttribute("userAgent", userAgent);
			try {
				user.setAttribute("reverseLookup",
						InetAddress.getByName(ac.getHttpServletRequest().getRemoteAddr()).getCanonicalHostName());
			} catch (UnknownHostException e) {
				// Nothing to do
			}

			UserKey uk = userInfoProvider.updateUser(user, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
					ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
			if (uk.getId() == null) {
				logger.warn("Failed to update last login date for user={}", id);
				throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
			}

			// sign in OK
			hts.removeAttribute("id");
			hts.removeAttribute("signInStep");
			SSOInfo sos = this.ssoManager.createSSOSession(id, user.getFirstName(), user.getLastName(),
					user.getDisplayName(), user.getEmail());

			String ssoId = sos.getSsoId();

			// set cookie
			SSOHelper.setCookie(ac.getHttpServletResponse(), SSOConstants.SSO_SSO_ID, ssoId, -1, "/");

			hts.setAttribute(SSOConstants.SSO_ID, sos.getId());
			hts.setAttribute(SSOConstants.SSO_SSO_ID, ssoId);
			result.put("ssoId", ssoId);
			resp.addMessage(PortalMessageID.LOGIN_SUCCESS.getMessage());
		}
		resp.addRow(result);
	}
	/**
	 * Authenticate current session user
	 * Use the current session user id and the password provided by crendential arg.
	 * 
	 * @param ctx, Context&lt;String&gt;. Will return a row with a String (the pass ticket)
	 * @param cred, Crendential. only password field will be used
	 * @deprecated
	 */
	public void checkCredential(final Context<String> ctx, final Credential cred) {
		if (cred == null) {
			logger.warn("Missing credential arg");
			throw ApplicationException.exception(AuthMessageID.AUTH_FAILED.getMessage().setSysId("sso"));
		}
		if (StringUtil.isEmpty(cred.getPwd())) {
			logger.warn("Missing credential pwd arg for user={}", cred.getId());
			throw ApplicationException.exception(AuthMessageID.AUTH_FAILED.getMessage().setSysId("sso"));
		}

		if (logger.isDebugEnabled()) {
			logger.debug(cred.toString());
		}

		AjaxContext ac = (AjaxContext) ctx;

		final ResponseHandler<String> resp = ctx.getResponseHandler();

		String ssoId = SSOHelper.getCookie(ac.getHttpServletRequest(), SSOConstants.SSO_SSO_ID);
		if (ssoId == null) {
			logger.warn("No sso session found!, ssoId={}", ssoId);
			throw ApplicationException.exception(AuthMessageID.AUTH_FAILED.getMessage().setSysId("sso"));
		}

		SSOInfo si = ssoManager.getSSOSession(ssoId);
		String id = si.getId();

		User user = userInfoProvider.readUser(id, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (user == null) {
			logger.warn("Unknown user with id={}", id);
			resp.addMessage(AuthMessageID.AUTH_FAILED.getMessage().setSysId("sso"));
			return;
		} else {
			if (user.getMissedLogin() >= 3) {
				logger.warn("Missed logins >= 3 for user={}", id);
				throw ApplicationException.exception(AuthMessageID.AUTH_FAILED.getMessage().setSysId("sso"));
			}
			if (!user.isActive()) {
				logger.warn("Login not active for user={}", id);
				resp.addMessage(AuthMessageID.AUTH_FAILED.getMessage().setSysId("sso"));
				return;
			}
		}
		// check hashed password
		if (!CommonChecksumFunction.SHA512.validateChecksum(cred.getPwd(), user.getPwd())) {
			logger.warn("Password do not match for user={}", id);

			user.setMissedLogin(user.getMissedLogin() + 1);
			UserKey uk = userInfoProvider.updateState(user, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
					ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
			if (uk.getId() == null) {
				logger.warn("Failed to lock login account after 3 unsuccessful login attempts for user={}", id);
				throw ApplicationException.exception(AuthMessageID.AUTH_FAILED.getMessage().setSysId("sso"));
			}

			throw ApplicationException.exception(AuthMessageID.AUTH_FAILED.getMessage().setSysId("sso"));
		}

		if (!checkDomainAccess(user.getFirmId(), ac)) {
			throw ApplicationException.exception(AuthMessageID.AUTH_FAILED.getMessage().setSysId("sso"));
		}

		user.setLastLoginDate(new Date());
		user.setMissedLogin(0);
		user.setAttribute("ip", ac.getHttpServletRequest().getRemoteAddr());
		user.setAttribute("domain", getDomainName(ac.getHttpServletRequest()));
		String userAgent = ac.getHttpServletRequest().getHeader(HTTP.USER_AGENT);

		if (userAgent == null) {
			userAgent = "";
		}
		user.setAttribute("userAgent", userAgent);
		try {
			user.setAttribute("reverseLookup",
					InetAddress.getByName(ac.getHttpServletRequest().getRemoteAddr()).getCanonicalHostName());
		} catch (UnknownHostException e) {
			// Nothing to do
		}

		UserKey uk = userInfoProvider.updateUser(user, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (uk.getId() == null) {
			logger.warn("Failed to update last login date for user={}", id);
			throw ApplicationException.exception(AuthMessageID.AUTH_FAILED.getMessage().setSysId("sso"));
		}

		resp.addMessage(AuthMessageID.AUTH_SUCCESS.getMessage().setSysId("sso"));
		String ticket = ssoManager.createTicket(ssoId);
		resp.addRow(ticket);
	}

	protected boolean isTwoStepsSignin(long firmId, AjaxContext ac) {
		boolean twoStepsSignin = true;
		Firm firm = userInfoProvider.readFirm(firmId, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (firm == null) {
			logger.warn("Firm (firmId={}) not found or there is permission problem!", firmId);
		} else if (firm.getAttribute("twoStepsSignin") != null) {
			twoStepsSignin = (Boolean) firm.getAttribute("twoStepsSignin");
			if (!twoStepsSignin) {
				// ignore challenge questions
				logger.info("Firm {} has disabled signin challenge questions!", firm.getFirmName());
			}
		}
		return twoStepsSignin;
	}

	protected boolean checkDomainAccess(long firmId, AjaxContext ac) {
		// does user allowed to sign in from this domain?
		String sd = getSSODomain(ac.getHttpServletRequest());
		Firm firm = userInfoProvider.readFirm(firmId, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (firm == null) {
			logger.warn("Firm (firmId={}) not found or there is permission problem!", firmId);
			return false;
		}
		if (firm.getAttribute("allowedDomains") == null) {
			logger.warn("Firm {} has no allowedDomains settings! Allowing access from domain {}", firm.getFirmName(),
					sd);
			return true;
		}

		String allowedDomains = (String) firm.getAttribute("allowedDomains");
		if (Strings.isNullOrEmpty(allowedDomains)) {
			logger.warn("Firm {} has not allowedDomains settings!", firm.getFirmName());
			logger.warn("Firm {} has no allowedDomains settings! Allowing access from domain {}", firm.getFirmName(),
					sd);
			return true;
		}
		String[] ds = allowedDomains.split(",");
		for (String d : ds) {
			if (d.equals("*") || sd.equalsIgnoreCase(d)) {
				logger.info("Firm {} allowed access from domain {}", firm.getFirmName(), d);
				return true;
			}
		}
		logger.warn("Firm {} is not allowed access from domain {}. Should use {}",
				new Object[] { firm.getFirmName(), sd, allowedDomains });
		return false;
	}

	protected boolean sqSetupRequired(UserQuestions uq) {
		return  (Strings.isNullOrEmpty(uq.getQ1()) || Strings.isNullOrEmpty(uq.getR1()) || Strings.isNullOrEmpty(uq.getQ2())
				|| Strings.isNullOrEmpty(uq.getR2()) || Strings.isNullOrEmpty(uq.getQ3())
				|| Strings.isNullOrEmpty(uq.getR3()));
	}
	protected boolean sqSetupRequired(User user) {
		return  (Strings.isNullOrEmpty(user.getQ1()) || Strings.isNullOrEmpty(user.getR1()) || Strings.isNullOrEmpty(user.getQ2())
				|| Strings.isNullOrEmpty(user.getR2()) || Strings.isNullOrEmpty(user.getQ3())
				|| Strings.isNullOrEmpty(user.getR3()));
	}
	public void getS2Info(final Context<String> ctx) {
		AjaxContext ac = (AjaxContext) ctx;

		HttpSession hts = ac.getHttpServletRequest().getSession(true);
		String id = (String) hts.getAttribute("id");
		String step = (String) hts.getAttribute("signInStep");
		if (Strings.isNullOrEmpty(id)) {
			logger.warn("Missing id in session (missing signin step1).");
			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}
		if (Strings.isNullOrEmpty(step)) {
			logger.warn("Missing step in session (missing signin step1).");
			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}

		if (!step.equals("1")) {
			logger.warn("bad signIn step in session (bad signin step1).");
			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}

		final ResponseHandler<String> resp = ctx.getResponseHandler();
		UserQuestions uq = userInfoProvider.readUserQuestions(id, ac.getRequest().getSessionId(),
				ac.getRequest().getReqId(), ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (uq == null) {
			logger.warn("Unknown user with id={}", id);
			resp.addMessage(AuthMessageID.LOGIN_FAILED.getMessage());
			return;
		}
		if (sqSetupRequired(uq)) {
			resp.addMessage(PortalMessageID.QUESTIONS_SETUP_REQUIRED.getMessage());
			return;
		}

		User user = userInfoProvider.readUser(id, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (user == null) {
			logger.warn("Unknown user with id={}", id);
			resp.addMessage(AuthMessageID.LOGIN_FAILED.getMessage());
			return;
		}

		if (user.isResetInProgress()) {
			resp.addMessage(PortalMessageID.QUESTIONS_SETUP_REQUIRED.getMessage());
			return;
		}

		String q = null;
		if (uq.getLastQ() <= 1) {
			q = uq.getQ1();
		} else if (uq.getLastQ() == 2) {
			q = uq.getQ2();
		} else {
			q = uq.getQ3();
		}

		if (Strings.isNullOrEmpty(q)) {
			logger.warn("Next question to ask is empty/null for user={}", id);
			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}
		resp.addRow(q);
		resp.addRow(uq.getAvatar());
	}

	public void signInS2(final Context<String> ctx, final QR qr) {
		AjaxContext ac = (AjaxContext) ctx;

		if (qr == null) {
			logger.warn("Missing qr arg");
			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}

		HttpSession hts = ac.getHttpServletRequest().getSession(true);
		String id = (String) hts.getAttribute("id");
		String step = (String) hts.getAttribute("signInStep");
		if (Strings.isNullOrEmpty(id)) {
			logger.warn("Missing id in session (missing signin step1).");
			throw ApplicationException.exception(PortalMessageID.LOGIN_TIMEOUT.getMessage());
		}
		if (Strings.isNullOrEmpty(step)) {
			logger.warn("Missing step in session (missing signin step1).");
			throw ApplicationException.exception(PortalMessageID.LOGIN_TIMEOUT.getMessage());
		}
		if (!step.equals("1")) {
			logger.warn("bad signIn step in session (bad signin step1).");
			throw ApplicationException.exception(PortalMessageID.LOGIN_TIMEOUT.getMessage());
		}

		String question = qr.getQ();
		if (StringUtil.isEmpty(question)) {
			logger.warn("Missing question arg for user={}", id);
			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}
		String answer = qr.getR();
		if (StringUtil.isEmpty(answer)) {
			logger.warn("Missing response arg for user={}", id);
			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}

		UserQuestions uq = userInfoProvider.readUserQuestions(id, ac.getRequest().getSessionId(),
				ac.getRequest().getReqId(), ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		User user = userInfoProvider.readUser(id, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (user == null) {
			logger.warn("Unknown user with id={}", id);
			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}
		if (user.getMissedLogin() >= 3) {
			logger.warn("Missed logins >= 3 for user={}", id);
			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}

		if (!checkDomainAccess(user.getFirmId(), ac)) {
			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}

		String rightAnswer = null;
		int qi = 0;
		if (!Strings.isNullOrEmpty(uq.getQ1()) && uq.getQ1().equals(question)) {
			rightAnswer = uq.getR1();
			qi = 2;
		} else if (!Strings.isNullOrEmpty(uq.getQ2()) && uq.getQ2().equals(question)) {
			rightAnswer = uq.getR2();
			qi = 3;
		} else if (!Strings.isNullOrEmpty(uq.getQ3()) && uq.getQ3().equals(question)) {
			rightAnswer = uq.getR3();
			qi = 1;
		} else {
			logger.warn("Unknown question provided for user={}, question={}", id, question);
			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}
		if (Strings.isNullOrEmpty(rightAnswer)) {
			logger.warn("No answer found in DB for user={}.", id);
			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}

		// the question will change next login
		// even if the answer of the current question is wrong.
		boolean isAnswerCorrect = true;
		// check DB hashed answer
		if (!CommonChecksumFunction.SHA512.validateChecksum(answer, rightAnswer)) {
			isAnswerCorrect = false;
			logger.warn("Answer do not match for user={}", id);
			user.setMissedLogin(user.getMissedLogin() + 1);
			UserKey uk = userInfoProvider.updateState(user, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
					ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
			if (uk.getId() == null) {
				logger.warn("Failed to lock login account after 3 unsuccessful login attempts for user={}", id);
				throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
			}
			if (user.getMissedLogin() >= 3) {
				String env = System.getProperty("env");
				// notify admin that a user has lock up his login
				mailer.sendMail(adminNotifEmails, user.getEmail() + " locked his login|Portal|" + env,
						user.getEmail() + " locked his login in Portal in the " + env + " environment!", fromRecipient);
			}
			user.setRevNo(uk.getRevNo());
		}

		if (isAnswerCorrect) {
			user.setLastLoginDate(new Date());
			user.setMissedLogin(0);
		}
		user.setLastQ(qi);
		user.setAttribute("ip", ac.getHttpServletRequest().getRemoteAddr());
		user.setAttribute("domain", getDomainName(ac.getHttpServletRequest()));
		String userAgent = ac.getHttpServletRequest().getHeader(HTTP.USER_AGENT);
		if (userAgent == null) {
			userAgent = "";
		}
		user.setAttribute("userAgent", userAgent);
		try {
			user.setAttribute("reverseLookup",
					InetAddress.getByName(ac.getHttpServletRequest().getRemoteAddr()).getCanonicalHostName());
		} catch (UnknownHostException e) {
			// Nothing to do
		}

		UserKey uk = userInfoProvider.updateUser(user, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (uk.getId() == null) {
			logger.warn("Failed to update last login date for user={}", id);
			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}

		if (!isAnswerCorrect) {
			throw ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}

		// sign in OK
		String ssoId = createSSOSession(hts, user, ac);
		final ResponseHandler<String> resp = ctx.getResponseHandler();
		resp.addRow(ssoId);
		resp.addMessage(PortalMessageID.LOGIN_SUCCESS.getMessage());
	}

	protected String createSSOSession(HttpSession hts, User user, AjaxContext ac) {
		hts.removeAttribute("id");
		hts.removeAttribute("signInStep");

		if (!checkDomainAccess(user.getFirmId(), ac)) {
			throw ApplicationException.exception(CommonMessageID.SERVICE_FAILURE.getMessage());
		}

		SSOInfo sos = this.ssoManager.createSSOSession(user.getId(), user.getFirstName(), user.getLastName(),
				user.getDisplayName(), user.getEmail());

		String ssoId = sos.getSsoId();

		// set cookie
		HttpServletResponse resp = ac.getHttpServletResponse();
		SSOHelper.setCookie(resp, SSOConstants.SSO_SSO_ID, ssoId, -1, "/");

		hts.setAttribute(SSOConstants.SSO_ID, sos.getId());
		hts.setAttribute(SSOConstants.SSO_SSO_ID, ssoId);
		return ssoId;
	}

	protected boolean hasSession(HttpServletRequest req) {
		String ssoId = SSOHelper.getCookie(req, SSOConstants.SSO_SSO_ID);
		return this.ssoManager.getSSOSession(ssoId) != null;
	}
	
	protected void assertSession(HttpServletRequest req) {
		if (!hasSession(req)) {
			HttpSession hts = req.getSession(false);
			String id = null;
			if (hts!=null) {
				id = (String) hts.getAttribute("id");
			}
			logger.warn("User has no session! userId=" +id);
			throw ApplicationException.exception(CommonMessageID.SERVICE_FAILURE.getMessage());
		}
	}
	
	public void setQRs(final Context<?> ctx, final UserQuestions qrs) {
		AjaxContext ac = (AjaxContext) ctx;

		if (qrs == null) {
			logger.warn("Missing QRs arg");
			throw ApplicationException.exception(PortalMessageID.QUESTIONS_SETUP_MISSING_INFO.getMessage());
		}

		HttpSession hts = ac.getHttpServletRequest().getSession(true);
		String id = (String) hts.getAttribute("id");
		if (Strings.isNullOrEmpty(id)) {
			logger.warn("Missing id in session (missing signin step1).");
			throw ApplicationException.exception(PortalMessageID.QUESTIONS_SETUP_MISSING_INFO.getMessage());
		}

		User user = userInfoProvider.readUser(id, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (user == null) {
			logger.warn("Unknown user with id={}", id);
			ApplicationException.exception(AuthMessageID.LOGIN_FAILED.getMessage());
		}

		if (!user.isResetInProgress() && !sqSetupRequired(user)) {
			assertSession(ac.getHttpServletRequest());
		}

		if (StringUtil.isEmpty(qrs.getQ1())) {
			logger.warn("Missing question 1 arg for user={}", qrs.getId());
			throw ApplicationException.exception(PortalMessageID.QUESTIONS_SETUP_MISSING_INFO.getMessage());
		}
		if (StringUtil.isEmpty(qrs.getR1())) {
			logger.warn("Missing response 1 arg for user={}", qrs.getId());
			throw ApplicationException.exception(PortalMessageID.QUESTIONS_SETUP_MISSING_INFO.getMessage());
		}
		if (StringUtil.isEmpty(qrs.getQ2())) {
			logger.warn("Missing question 2 arg for user={}", qrs.getId());
			throw ApplicationException.exception(PortalMessageID.QUESTIONS_SETUP_MISSING_INFO.getMessage());
		}
		if (StringUtil.isEmpty(qrs.getR2())) {
			logger.warn("Missing response 2 arg for user={}", qrs.getId());
			throw ApplicationException.exception(PortalMessageID.QUESTIONS_SETUP_MISSING_INFO.getMessage());
		}
		if (StringUtil.isEmpty(qrs.getQ3())) {
			logger.warn("Missing question 3 arg for user={}", qrs.getId());
			throw ApplicationException.exception(PortalMessageID.QUESTIONS_SETUP_MISSING_INFO.getMessage());
		}
		if (StringUtil.isEmpty(qrs.getR3())) {
			logger.warn("Missing response 3 arg for user={}", qrs.getId());
			throw ApplicationException.exception(PortalMessageID.QUESTIONS_SETUP_MISSING_INFO.getMessage());
		}

		// must be different questions
		if (qrs.getQ1().equalsIgnoreCase(qrs.getQ2()) || qrs.getQ1().equalsIgnoreCase(qrs.getQ3())
				|| qrs.getQ2().equalsIgnoreCase(qrs.getQ3())) {
			logger.warn("Questions must be differents for user={}", qrs.getId());
			throw ApplicationException.exception(PortalMessageID.QUESTIONS_SETUP_NOT_UNIQUES.getMessage());
		}

		if (StringUtil.isEmpty(qrs.getAvatar())) {
			logger.warn("Missing avatar arg for user={}", qrs.getId());
			throw ApplicationException.exception(PortalMessageID.QUESTIONS_SETUP_MISSING_INFO.getMessage("avatar"));
		}

		qrs.setRevNo(user.getRevNo());
		qrs.setId(id);
		qrs.setR1(CommonChecksumFunction.SHA512.generateChecksum(qrs.getR1()));
		qrs.setR2(CommonChecksumFunction.SHA512.generateChecksum(qrs.getR2()));
		qrs.setR3(CommonChecksumFunction.SHA512.generateChecksum(qrs.getR3()));

		UserKey uk = userInfoProvider.updateQuestions(qrs, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (uk.getId() == null) {
			logger.warn("Failed to set questions for user={}", id);
			throw ApplicationException.exception(PortalMessageID.QUESTIONS_SETUP_FAILED.getMessage());
		}

		user = userInfoProvider.readUser(id, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		user.setMissedLogin(0);
		user.setResetInProgress(false);

		uk = userInfoProvider.updateUser(user, ac.getRequest().getSessionId(), ac.getRequest().getReqId(),
				ac.getRequest().getLang(), getDomainName(ac.getHttpServletRequest()));
		if (uk.getId() == null) {
			logger.warn("Failed to reset login user={}", id);
			throw ApplicationException.exception(PortalMessageID.QUESTIONS_SETUP_FAILED.getMessage());
		}

		String ssoId = SSOHelper.getCookie(ac.getHttpServletRequest(), SSOConstants.SSO_SSO_ID);
		if (ssoId == null || ssoManager.getSSOSession(ssoId) == null) {
			createSSOSession(hts, user, ac);
		}
	}

	protected String getDomainName(HttpServletRequest req) {
		@SuppressWarnings("unchecked")
		Enumeration<String> snl = req.getHeaders("host");
		String dn = null;
		if (snl != null && snl.hasMoreElements()) {
			dn = snl.nextElement();
		}
		if (Strings.isNullOrEmpty(dn)) {
			dn = req.getServerName();
		}
		return dn;
	}

	public void setAdminNotifEmails(String adminNotifEmails) {
		this.adminNotifEmails = adminNotifEmails;
	}

	public void setFromRecipient(String fromRecipient) {
		this.fromRecipient = fromRecipient;
	}

	public void setUserInfoProvider(UserInfoProvider userInfoProvider) {
		this.userInfoProvider = userInfoProvider;
	}

	public void setAuthInfoProvider(AuthInfoProvider authInfoProvider) {
		this.authInfoProvider = authInfoProvider;
	}

	public String getRegistrationDisabledDomains() {
		return registrationDisabledDomains;
	}

	public void setRegistrationDisabledDomains(String registrationDisabledDomains) {
		this.registrationDisabledDomains = registrationDisabledDomains;
	}
}
